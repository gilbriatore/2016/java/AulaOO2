package br.edu.up;

import java.util.ArrayList;
import java.util.List;
import br.edu.up.tipo.Pessoa;

public class Programa {

	public static void main(String[] args){
	
		Pessoa p1 = new Pessoa();
		p1.setNome("Jo�o");
		p1.setIdade(25);
		
		Pessoa p2 = new Pessoa();
		p2.setNome("Paulo");
		p2.setIdade(17);

		List<Pessoa> lista = new ArrayList<Pessoa>();
		lista.add(p1);
		lista.add(p2);
		
		//Terceira forma de pegar as pessoas da lista;
		for (Pessoa pessoa : lista) {
			System.out.println("Nome: " + pessoa.getNome());
			System.out.println("� maior de idade: " 
			                      + pessoa.isMaiorDeIdade());
		}
		
		//Primeira forma de pegar as pessoas da lista;
//		int contador = 0;
//		while(contador < lista.size()){	
//			Pessoa p = lista.get(contador);
//			System.out.println("Nome: " + p.getNome()); 
//			contador++;
//		}
		
		
		//Segunda forma de pegar as pessoas da lista;
//		for (int i = 0; i < lista.size(); i++) {
//			Pessoa p = lista.get(i);
//			System.out.println("Nome: " + p.getNome()); 
//		}
		
	}
}