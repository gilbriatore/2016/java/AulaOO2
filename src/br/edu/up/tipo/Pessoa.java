package br.edu.up.tipo;

public class Pessoa {

	private String nome;
	private int idade;

	public boolean isMaiorDeIdade(){
		boolean isMaior = false;
		if (idade >= 18){
			isMaior = true;
		}
		return isMaior;
	}
	
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getIdade() {
		return idade;
	}

	public void setIdade(int idade) {
		this.idade = idade;
	}

}